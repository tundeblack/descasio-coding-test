import React from "react";
import { hot } from "react-hot-loader";

const Busy = (props) => 
{
    return (
        <div className = "text-center">
            <i className = "fa fa-spinner fa-pulse fa-2x" style = {{color:"rgb(15, 101, 135)"}}>
                { (props.status) ? props.status : "" }
            </i>
        </div>
    );
}

export default hot(module)(Busy);
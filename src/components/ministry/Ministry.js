import React from "react";
import { hot } from "react-hot-loader";
import MinistryHeader from "./MinistryHeader";
import MinistryBody from "./MinistryBody.js";


const Ministry = (props) => 
{
    return (
        <section className = "my-4">
            <MinistryHeader 
                ministryName = { props.name }
            />
            <MinistryBody 
                ministerName = { props.ministerName }
                ministerStateOfOrigin = { props.ministerStateOfOrigin }
                address = { props.address }
                website = { props.website }
            />
        </section>
    );
}

export default hot(module)(Ministry);
import React from "react";
import { hot } from "react-hot-loader";
import AppConfig from "../../config/AppConfig.js";
import Image from "../../components/image/Image.js";


const MinistryHeader = (props) => 
{
    return (
        <header>
            <div className = "row align-items-center mb-3">
                <div 
                    className = "col-3 pr-2 text-right"
                >
                    <Image 
                        src = { `${AppConfig.imagesDir}/coat_of_arms.png`  }
                        style = {{
                            width: "80px",
                            height: "auto",
                            borderLeft: "3px solid red"
                        }}
                        classes = "pl-2"
                    />
                </div>
                <div className = "col-9">
                    <h3 className = "my-0">{ props.ministryName }</h3>
                </div>
            </div>
        </header>
    );
}

export default hot(module)(MinistryHeader);
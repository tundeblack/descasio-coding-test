import React, { useState} from "react";
import { hot } from "react-hot-loader";
import Anchor from "../anchor/Anchor.js";


const MinistryBody = (props) => 
{
    const [ showMoreInfo, setShowMoreInfo ] = useState(false);


    return (
        <section>
            <div className = "row my-2">
                <div className = "col-3 text-right pr-2">
                    <h6 className = "my-0">Minister:</h6>
                </div>
                <div className = "col-9">{ props.ministerName }</div>
            </div>
            <div className = "row my-2">
                <div className = "col-3 text-right pr-2">
                    <h6 className = "my-0">Origin:</h6>
                </div>
                <div className = "col-9">{ props.ministerStateOfOrigin }</div>
            </div>
            {
                showMoreInfo ?
                    <React.Fragment>
                        <div className = "row my-2">
                            <div className = "col-3 text-right pr-2">
                                <h6 className = "my-0">Address:</h6>
                            </div>
                            <div className = "col-9">{ props.address }</div>
                        </div>
                        <div className = "row my-2">
                            <div className = "col-3 text-right pr-2">
                                <h6 className = "my-0">Website:</h6>
                            </div>
                            <div className = "col-9">{ props.website }</div>
                        </div>
                    </React.Fragment>
                :
                    null
            }
            <div className = "my-3 text-right">
                <Anchor 
                    linkTo = "#"
                    content = { 
                        <React.Fragment>
                            More Info&nbsp;{
                                showMoreInfo ? 
                                    <i className = "fa fa-caret-up"></i>
                                :
                                    <i className = "fa fa-caret-down"></i>     
                            }
                        </React.Fragment>
                    }
                    onClickCallback = {
                        (e) => 
                        {
                            e.preventDefault();
                            setShowMoreInfo(!showMoreInfo);
                        }
                    }
                />
            </div>
        </section>
    );
}

export default hot(module)(MinistryBody);
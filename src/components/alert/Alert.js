import React, { useState, useEffect } from "react";
import { hot } from "react-hot-loader";

function Alert(props)
{
    const [ visible, setVisible ] = useState(true);

    let classes = "";
    if (props.type === "error")
    {
        classes = "alert alert-danger";
    } else if (props.type === "success")
    {
        classes = "alert alert-success";
    }

    useEffect(
        () => 
        {
            if (props.ephemeral == true)
            {
                setTimeout(
                    () =>
                    {
                        setVisible(false);
                        if (props.onDisappear) 
                        {
                            props.onDisappear();
                        }
                    },
                    (props.timeout)? props.timeout : 2500
                )
            }
        },
        []
    )

    return (
        <React.Fragment>
            {
                (visible)?
                    <div className = { classes }>
                        <div className = "mb-0">{ props.message }</div>
                    </div>
                :
                    null
            }
        </React.Fragment>
    );
}

export default hot(module)(Alert);
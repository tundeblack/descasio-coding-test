import React from 'react';
import { hot } from "react-hot-loader";


const Image = (props) => 
{
    let { imageSrc, imageAltText, classes, ...others } = props;
    
    return (
        <img 
            src = { imageSrc } 
            alt = { imageAltText } 
            className = { "image " + ((classes) ? classes : "") }
            { ...others } 
        />
    );
}

export default hot(module)(Image);
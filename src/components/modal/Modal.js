import React from "react";
import { hot } from "react-hot-loader";


const Modal = (props) => 
{
    return (
        <div 
            id = { props.id }
            className = { `modal fade ${props.classes ? props.classes : ""}` }
            role = "dialog" 
            arial-labelledby = "modalTitle" 
            aria-hidden = "true"
            data-backdrop = { props.dataBackdrop }
            data-keyboard = { props.dataKeyboard }
        >
            <div className = { `modal-dialog ${(props.largeSize && props.largeSize == true) ? "modal-lg" : ""}` } role = "document">
                <div className = "modal-content" style = { props.style }>
                    <div className = "modal-header pb-1">
                        <h5 className = "modal-title" id = "modalTitle">{ props.title }</h5>
                        <button 
                            id = { props.closeId ? props.closeId : "close" }
                            className = "btn btn-light btn-sm close p-2" 
                            data-dismiss = "modal"
                            aria-hidden = "true"
                            onClick = {
                                (e) => 
                                {
                                    if (props.onCloseCallback && (typeof props.onCloseCallback == "function"))
                                    {
                                        props.onCloseCallback();
                                    }
                                }
                            }
                        >X</button>
                    </div>
                    <div className = "modal-body">
                        { props.children }
                        { 
                            props.errorMessage ?
                                <div className="alert alert-danger w-100">{ props.errorMessage }</div>
                            :
                                null      
                        }
                        { 
                            props.successMessage ?
                                <div className="alert alert-success w-100">{ props.successMessage }</div>
                            :
                                null      
                        }
                    </div>
                    {
                        props.includeFooter ?
                            <div className = "modal-footer">
                                <button
                                    onClick = { props.actionBtnCallback }
                                    busy = { props.isBusy }
                                    classes = "btn btn-primary"
                                >
                                    { props.actionBtnText }
                                </button>
                            </div>
                        :
                            null
                    }
                </div>
            </div>
        </div>
    );
}

export default hot(module)(Modal);
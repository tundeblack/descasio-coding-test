import React, { useEffect } from "react";
import { connect } from "react-redux";
import { hot } from "react-hot-loader";
import { Switch, Route} from "react-router-dom";
import Home from "./views/Home.js";
import Alert from "./components/alert/Alert.js";
import {
    makeClearAppErrorMsgAction,
    makeClearAppMsgAction
} from "./redux/ActionCreators.js";


const app = (props) => 
{
    return (
        <section>
            {
                (props.appErrorMsg)?
                    <div className = "sticky-top" style={{width:"100%",position:"fixed",left:"0",top:"0",zIndex:"10000000"}}>
                        <div className="row justify-content-center">
                            <div className="col-md-5">
                                <Alert 
                                    type = "error"
                                    message = { props.appErrorMsg }
                                    ephemeral = { true }
                                    onDisappear = {
                                        () => 
                                        {
                                            props.dispatch(makeClearAppErrorMsgAction());
                                        }
                                    }
                                />
                            </div>
                        </div>
                    </div>
                :
                    null
            }
            {
                (props.appMsg)?
                    <div className = "sticky-top" style={{width:"100%",position:"fixed",left:"0",top:"0",zIndex:"100000000"}}>
                        <div className="row justify-content-center">
                            <div className="col-md-5">
                                <Alert 
                                    type = "success"
                                    message = { props.appMsg }
                                    ephemeral = { true }
                                    onDisappear = {
                                        () => 
                                        {
                                            props.dispatch(makeClearAppMsgAction());
                                        }
                                    }
                                />
                            </div>
                        </div>
                    </div>
                :
                    null
            }
            <Switch>
                <Route 
                    exact
                    path = "/"
                    render = {
                        (props) => 
                        {
                            return <Home 
                                routerProps = { props }              
                            />
                        }
                    }
                />
            </Switch>
        </section>
    );
}

const DescasioApp = connect(
    state => 
    {
        return { 
            appErrorMsg: state.appErrorMsg,
            appMsg: state.appMsg,
        };
    },
    (dispatch) => 
    {
        return { dispatch };
    }
)(app);

export default hot(module)(DescasioApp);
import React from "react";
import ReactDom from "react-dom";
import { HashRouter } from "react-router-dom";
import { Provider } from "react-redux";
import DescasioApp from "./DescasioApp";
import { reduxStore, persistor } from "./redux/Store.js";
import { PersistGate } from "redux-persist/integration/react";
import Busy from "./components/busy/Busy.js";


ReactDom.render(
    <HashRouter>
        <Provider store = { reduxStore }>
            <PersistGate loading = {<Busy />} persistor = { persistor }>
                <DescasioApp />
            </PersistGate>
        </Provider>
    </HashRouter>,
    document.getElementById("appRoot")
);
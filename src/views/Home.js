import React, { useEffect } from "react";
import { hot } from "react-hot-loader";
import { connect } from "react-redux";
import { 
    makeSetMinistriesListAction,
    makeLoadMinistriesFromFileAction, 
    makeSetAppErrorMsgAction, 
} from "../redux/ActionCreators.js";
import { ministries } from "../../public/data/ministries.js";
import Ministry from "../components/ministry/Ministry.js";
import Anchor from "../components/anchor/Anchor.js";
import Modal from "../components/modal/Modal.js";


const Home = (props) => 
{
    useEffect(
        () => 
        {
            props.dispatch(
                makeSetMinistriesListAction(ministries)
            );
        },
        []
    );


    return (
        <React.Fragment>
            <section className = "container p-4">
                <div className = "row justify-content-center">
                    <div className = "col-md-8">

                        <h1 className = "mb-2">Ministries and Federal Parastatals</h1>
                        <hr className = "mb-0" />
                        <div className = "text-right mt-1 mb-5">
                            <Anchor 
                                linkTo = "#"
                                content = {
                                    <React.Fragment>
                                        { <i className = "fa fa-file" style = {{color: "orange"}}>&nbsp;</i> }
                                        Data
                                    </React.Fragment>
                                }
                                data-toggle = "modal"
                                data-target = "#dlgBox"
                                style = {{
                                    fontSize: "1rem",
                                    fontWeight: "900"
                                }}
                            />
                        </div>

                        <div className = "my-4">
                            {
                                props.ministries ? 
                                    props.ministries.map(
                                        (m, i) => 
                                        {
                                            return (
                                                <React.Fragment key = { i }>
                                                    <Ministry 
                                                        name = { m.name }
                                                        ministerName = { m.minister }
                                                        ministerStateOfOrigin = { m.origin }
                                                        address = { m.address }
                                                        website = { m.website }
                                                    />
                                                    { 
                                                        (i < props.ministries.length - 1) ? 
                                                            <hr className = "my-2" />
                                                        :
                                                            null
                                                    }       
                                                </React.Fragment>
                                            );
                                        }
                                    )
                                :
                                    null
                            }
                        </div>
                    </div>
                </div>
            </section>
            <Modal 
                id = "dlgBox" 
                title = "Read Ministries From File"
                dataBackdrop = "static"
                dataKeyboard = { false }
                closeId = "closeDlgBox"
            >
                <div className = "my-4">
                    <div className = "bg-light p-2 mb-4 border rounded">
                        <h5 className = "mt-3 mb-2">Instructions</h5>
                        <p>Choose a data file below.</p> 
                        <ol>
                            <li>The data file must be a plain text file containing a JSON object.</li> 
                            <li>The JSON object must be an array of Ministry objects.</li>
                            <li>Each Ministry object must have <strong>name, minister, origin, address and website as properties</strong>.</li>
                        </ol>
                    </div>
                    <div>
                        <input 
                            type = "file"
                            id = "datafile"
                        />
                    </div>
                    <hr className = "my-4" />
                    <div className = "text-right">
                        <button
                            className = "btn btn-primary"
                            onClick = {
                                (e) => 
                                {
                                    let input = document.getElementById("datafile");
                                    if (input.files[0])
                                    {
                                        props.dispatch(
                                            makeLoadMinistriesFromFileAction(
                                                {
                                                    datafile: input.files[0],
                                                    closeModal: () => 
                                                    {
                                                        document.getElementById("closeDlgBox").click();
                                                    }
                                                }
                                            )
                                        );
                                    } else 
                                    {
                                        props.dispatch(
                                            makeSetAppErrorMsgAction("No file was selected.")
                                        );
                                    }
                                }
                            }
                        >Read File</button>
                    </div>
                </div>
            </Modal>
        </React.Fragment>
    );
}

export default hot(module)(
    connect(
        state =>
        {
            return {
                ministries: state.ministriesList
            }
        },
        dispatch =>
        {
            return { dispatch };
        }
    )(Home)
);
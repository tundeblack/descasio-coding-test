const AppConfig = {
    imagesDir: "/assets/images",
    dataFile: "/data/ministries.js"
};

export default AppConfig;
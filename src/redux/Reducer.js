"use-strict";

import * as Actions from "../constants/Actions.js";


const initialState = {
    ministriesList: [],

    appMsg: "",
    appErrorMsg: ""
};

const reducer = (state = initialState, action) => 
{
    const objAssign = (newState) =>
    {
        return Object.assign({}, state, newState);
    }
    
    switch (action.type)
    {
        case Actions.SET_MINISTRIES_LIST:
            return objAssign(
                {
                    ministriesList: action.payload
                }
            );
        
        case Actions.SET_APP_MESSAGE:
            return objAssign(
                {
                    appMsg: action.payload
                }
            );
        case Actions.SET_APP_ERROR_MESSAGE:
            return objAssign(
                {
                    appErrorMsg: action.payload
                }
            );
        case Actions.CLEAR_APP_MESSAGE:
            return objAssign(
                {
                    appMsg: ""
                }
            );
        case Actions.CLEAR_APP_ERROR_MESSAGE:
            return objAssign(
                {
                    appErrorMsg: ""
                }
            );
        
        default:
            return state;
    }
}

export default reducer;
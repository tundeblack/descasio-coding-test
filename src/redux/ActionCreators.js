import * as Action from "../constants/Actions.js";


export const makeSetMinistriesListAction = (payload) => 
{
    return {
        type: Action.SET_MINISTRIES_LIST,
        payload
    };
}

export const makeLoadMinistriesFromFileAction = ({ datafile, closeModal }) =>
{
    return {
        type: Action.LOAD_MINISTRIES_FROM_FILE,
        payload: {
            datafile, 
            closeModal
        }
    };
}

export const makeSetAppErrorMsgAction = (payload) =>
{
    return {
        type: Action.SET_APP_ERROR_MESSAGE,
        payload
    };
}
export const makeClearAppErrorMsgAction = () => 
{
    return { type: Action.CLEAR_APP_ERROR_MESSAGE };
}
export const makeClearAppMsgAction = () => 
{
    return { type: Action.CLEAR_APP_MESSAGE };
}

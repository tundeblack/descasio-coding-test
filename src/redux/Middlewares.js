import * as Actions from "../constants/Actions.js";
import { 
    makeSetMinistriesListAction,
    makeSetAppErrorMsgAction
} from "../redux/ActionCreators.js";


export const loadMinistriesFile = ({ getState, dispatch }) => 
{
    return (next) => 
    {
        return (action) => 
        {
            const dispatchError = (error) => 
            {
                dispatch(
                    makeSetAppErrorMsgAction(
                        error.message
                    )
                );
            }

            if (action.type == Actions.LOAD_MINISTRIES_FROM_FILE)
            {
                // Read data file and parse JSON object in the file.
                // Then, dispatch a SET_MINISTRIES_LIST action.            
                
                if (action.payload && action.payload !== "")
                {
                    try 
                    {
                        if (action.payload.datafile.type !== "text/plain")
                        {
                            throw new Error("File must be a plain text file.");
                        }
                        
                        const reader = new FileReader();
                        
                        reader.readAsText(action.payload.datafile);
                        reader.onload = () => 
                        {
                            try 
                            {
                                const data = JSON.parse(reader.result);
                                if (!data || !Array.isArray(data))
                                {
                                    throw new Error("Invalid text found in data file. Data file must contain a JSON string with an array of ministry objects.");
                                }
                                
                                dispatch(
                                    makeSetMinistriesListAction(data)
                                );
                                
                                if (action.payload.closeModal && 
                                (typeof action.payload.closeModal == "function")
                                )
                                {
                                    action.payload.closeModal();
                                }
                                
                            } catch (error)
                            {
                                dispatchError(error);
                            }
                        }
                        reader.onerror = () => dispatchError(reader.error)
                    } catch (error)
                    {
                        dispatchError(error);
                    }
                }
            }
            
            return next(action);
        }
    }
}

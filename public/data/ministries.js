export const ministries = [
    {
        name: "Ministry of Justice",
        minister: "Abubakar Malami",
        origin: "Kebbi State",
        address: "Federal Ministry of Justice, Federal Secretariat Towers (5th & 10th floors), Shehu Shagari Way, Central Area, Abuja. Federal Capital Territory. Nigeria. ", 
        website: "www.justice.gov.ng" 
    }, 
    { 
        name: "Ministry of Transportation",
        minister: "Rotimi Amaechi",
        origin: "Rivers State",
        address: "Federal Ministry of Justice, Federal Secretariat Towers (5th & 10th floors), Shehu Shagari Way, Central Area, Abuja. Federal Capital Territory. Nigeria. ", 
        website: "www.transport.gov.ng" 
    }
]; 
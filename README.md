# Descasio Coding Test

### Setup
Clone repo to local machine by running 

```git clone git@gitlab.com:tundeblack/descasio-coding-test.git``` 

Then install all dependencies by running: ```npm install```. After that, run ```npm run dev-server``` to start the local server and go to ```localhost:3000``` to access the application.

The data file containing initial loaded data can be found here ```public/data/ministries.js```.

A test data file for Dynamic ministries data loading is included in the root directory with file name ```descasio_datafile.txt```.

Feel free to contact me at [olatundesamuel2018@gmail.com](olatundesamuel2018@gmail.com) in case you have any questions.

Thank you.